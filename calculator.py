#  Simple function calculator

import sys

def calculator(operator, x, y):
    if operator == "add":
        return (x + y)
    elif operator == "sub":
        return (x - y)
    elif operator == "mul":
        return (x * y)
    elif operator == "div":
        return (x / y)
    else:
        return None

operator = sys.argv[1]
x = int(sys.argv[2])
y = int(sys.argv[3])
print(calculator(operator, x, y))

